#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Purchase Subsequent',
    'name_de_DE': 'Einkauf rückwirkende Erfassung',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''
    - Allows to enter purchase orders subsequently
''',
    'description_de_DE': '''
    - Ermöglicht die rückwirkende Erfassung von Einkäufen
''',
    'depends': [
        'purchase',
    ],
    'xml': [
    ],
    'translation': [
    ],
}
